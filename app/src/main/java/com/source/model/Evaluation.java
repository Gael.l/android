package com.source.model;

public class Evaluation {
    private String idTrajet;
    private String idUser;
    private int score;

    public Evaluation () {}
    public Evaluation(String idTrajet, String idUser, int score) {
        this.idTrajet = idTrajet;
        this.idUser = idUser;
        this.score = score;
    }

    public String getIdTrajet() {
        return idTrajet;
    }

    public void setIdTrajet(String idTrajet) {
        this.idTrajet = idTrajet;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
