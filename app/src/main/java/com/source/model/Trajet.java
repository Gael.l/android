package com.source.model;

import java.util.ArrayList;
import java.util.Date;

public class Trajet {

    private String id;
    private String createurId;
    private ArrayList<String> participants = new ArrayList<>();
    private String vehicule;
    private Date depart;
    private Date arrivee;
    private Date heureDepart;
    private Date heureArrivee;
    private int retard;
    private int nbrPlaces;
    private boolean isVoitue;
    private boolean isAutoroute;
    private int prix;
    private String distance;
    private String duree;
    private String adresseDepart;
    private String adresseArivee;
    private int secondes;

    public Trajet() {
    }

    public Trajet(String id, String createurId, ArrayList<String> participants, String vehicule, Date depart, Date arrivee, Date heureDepart, Date heureArrivee, int retard, int nbrPlaces, boolean isVoitue, boolean isAutoroute, int prix, String distance, String duree, String adresseDepart, String adresseArivee) {
        this.id = id;
        this.createurId = createurId;
        this.participants = participants;
        this.vehicule = vehicule;
        this.depart = depart;
        this.arrivee = arrivee;
        this.heureDepart = heureDepart;
        this.heureArrivee = heureArrivee;
        this.retard = retard;
        this.nbrPlaces = nbrPlaces;
        this.isVoitue = isVoitue;
        this.isAutoroute = isAutoroute;
        this.prix = prix;
        this.distance = distance;
        this.duree = duree;
        this.adresseDepart = adresseDepart;
        this.adresseArivee = adresseArivee;
    }

    public Trajet(String vehicule, Date depart, int prix, String duree, String adresseDepart, String adresseArivee) {
        this.vehicule = vehicule;
        this.depart = depart;
        this.prix = prix;
        this.duree = duree;
        this.adresseDepart = adresseDepart;
        this.adresseArivee = adresseArivee;
    }

    public Trajet(String createurId, Date depart, int retard, int nbrPlaces, String vehicule, boolean isVoitue, boolean isAutoroute, int prix, String duree, int secondes, String distance, String adresseDepart, String adresseArrivee) {
        this.createurId = createurId;
        this.depart = depart;
        this.retard = retard;
        this.nbrPlaces = nbrPlaces;
        this.vehicule = vehicule;
        this.isVoitue = isVoitue;
        this.isAutoroute = isAutoroute;
        this.prix = prix;
        this.duree = duree;
        this.secondes = secondes;
        this.distance = distance;
        this.adresseDepart = adresseDepart;
        this.adresseArivee = adresseArrivee;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreateurId() {
        return createurId;
    }

    public void setCreateurId(String createurId) {
        this.createurId = createurId;
    }

    public ArrayList<String> getParticipants() {
        return participants;
    }

    public void setParticipants(ArrayList<String> participants) {
        this.participants = participants;
    }

    public String getVehicule() {
        return vehicule;
    }

    public void setVehicule(String vehicule) {
        this.vehicule = vehicule;
    }

    public Date getDepart() {
        return depart;
    }

    public void setDepart(Date depart) {
        this.depart = depart;
    }

    public Date getArrivee() {
        return arrivee;
    }

    public void setArrivee(Date arrivee) {
        this.arrivee = arrivee;
    }

    public Date getHeureDepart() {
        return heureDepart;
    }

    public void setHeureDepart(Date heureDepart) {
        this.heureDepart = heureDepart;
    }

    public Date getHeureArrivee() {
        return heureArrivee;
    }

    public void setHeureArrivee(Date heureArrivee) {
        this.heureArrivee = heureArrivee;
    }

    public int getRetard() {
        return retard;
    }

    public void setRetard(int retard) {
        this.retard = retard;
    }

    public int getNbrPlaces() {
        return nbrPlaces;
    }

    public void setNbrPlaces(int nbrPlaces) {
        this.nbrPlaces = nbrPlaces;
    }

    public boolean isVoitue() {
        return isVoitue;
    }

    public void setVoitue(boolean voitue) {
        isVoitue = voitue;
    }

    public boolean isAutoroute() {
        return isAutoroute;
    }

    public void setAutoroute(boolean autoroute) {
        isAutoroute = autoroute;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    public String getAdresseDepart() {
        return adresseDepart;
    }

    public void setAdresseDepart(String adresseDepart) {
        this.adresseDepart = adresseDepart;
    }

    public String getAdresseArivee() {
        return adresseArivee;
    }

    public void setAdresseArivee(String adresseArivee) {
        this.adresseArivee = adresseArivee;
    }

    public int getSecondes() {
        return secondes;
    }

    public void setSecondes(int secondes) {
        this.secondes = secondes;
    }
}
