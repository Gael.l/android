package com.source.model;

import java.util.ArrayList;

public class User {
    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private String ville;
    private String phone;
    private float score;
    private String pictureUrl;
    private String picturePath;
    private ArrayList<Evaluation> userTrajetEvaluate = new ArrayList<>();

    public User(String id, String firstName, String lastName, String email, String ville, String phone, float score, String pictureUrl, String picturePath, ArrayList<Evaluation> userTrajetEvaluate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.ville = ville;
        this.phone = phone;
        this.score = score;
        this.pictureUrl = pictureUrl;
        this.picturePath = picturePath;
        this.userTrajetEvaluate = userTrajetEvaluate;
    }

    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public ArrayList<Evaluation> getUserTrajetEvaluate() {
        return userTrajetEvaluate;
    }

    public void setUserTrajetEvaluate(ArrayList<Evaluation> userTrajetEvaluate) {
        this.userTrajetEvaluate = userTrajetEvaluate;
    }
}
