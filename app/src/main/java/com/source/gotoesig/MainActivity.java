package com.source.gotoesig;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

/*
    Created By
    Ferdousur Rahman Sarker
    www.androstock.com
*/

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
