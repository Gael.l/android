package com.source.gotoesig.ui.trajets;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.source.gotoesig.R;
import com.source.model.Trajet;

import java.util.ArrayList;
import java.util.List;

public class TrajetsFragment extends Fragment {


    private TrajetsViewModel trajetsViewModel;
    private List<Trajet> trajetsProposes = new ArrayList<>();
    private List<Trajet> trajetsReserves = new ArrayList<>();
    private FirebaseFirestore db;
    private FirebaseAuth mauth;
    private static final String TAG = "TrajetFragment";
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        trajetsViewModel = ViewModelProviders.of(this).get(TrajetsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_trajets, container, false);
        final TextView textView = root.findViewById(R.id.text_search);
        final RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        final RecyclerView recyclerView1 = root.findViewById(R.id.recyclerView1);
        trajetsViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                db = FirebaseFirestore.getInstance();
                mauth = FirebaseAuth.getInstance();
                db.collection("trajets")
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        Log.d(TAG, document.getId() + " => " + document.getData());

                                        /** On convertit le document en objet Java Trajet */
                                        Trajet trajet = document.toObject(Trajet.class);
                                        trajet.setId(document.getId());

                                        /** Créations des booleans permettant de verifier la présence d'un trajet dans la liste des trajets a chercher*/
                                        boolean isCurrentUserParticipant =  trajet.getParticipants() != null && trajet.getParticipants().stream().anyMatch(id -> id.equals(mauth.getUid()));
                                        boolean isCurrentUserCreator = trajet.getCreateurId().equals(mauth.getUid());

                                        /** Condition pour les trajets proposés */
                                        if(isCurrentUserCreator) {
                                            trajetsProposes.add(trajet);
                                        }

                                        /** Condition pour les trajets reservés */
                                        if(isCurrentUserParticipant){
                                            trajetsReserves.add(trajet);
                                        }
                                    }
                                } else {
                                    Log.d(TAG, "Error getting documents: ", task.getException());
                                }
                                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                                recyclerView.setAdapter(new TrajetsAdapter(getContext(), trajetsProposes));
                                recyclerView.setNestedScrollingEnabled(false);

                                recyclerView1.setLayoutManager(new LinearLayoutManager(getContext()));
                                recyclerView1.setAdapter(new TrajetsAdapter(getContext(), trajetsReserves));
                                recyclerView1.setNestedScrollingEnabled(false);
                            }
                        });
                textView.setText(s);
            }
        });
        return root;
    }
}