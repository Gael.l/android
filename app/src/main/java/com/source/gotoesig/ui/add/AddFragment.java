package com.source.gotoesig.ui.add;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.source.gotoesig.R;
import com.source.model.Trajet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutionException;

public class AddFragment extends DialogFragment {

    private AddViewModel addViewModel;
    private static final String TAG = "AddFragment";
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        // déclaration des variables
        addViewModel = ViewModelProviders.of(this).get(AddViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_add, container, false);
        Button add = root.findViewById(R.id.add);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        final EditText depart;
        final AutoCompleteTextView start;
        final DatePicker dateDepart;
        final Button btnDateDepart;
        final TextView textDateDepart;
        final TimePicker heureDepart;
        final Button btnHeureDepart;
        final TextView textHeureDepart;
        final EditText places;
        final EditText retard;
        final EditText prix;
        final TextInputLayout prixLayout;
        final CheckBox autoroute;
        final ImageView ic_autoroute;

        final Spinner vehicle;
        final LinearLayout ajout;
        final LinearLayout info;
        final TextView tDuree;
        final TextView tDist;

        AutoCompleteTextView autoCompleteTextView=root.findViewById(R.id.autocomplete);
        autoCompleteTextView.setAdapter(new PlaceAutoSuggestAdapter(getContext(),android.R.layout.simple_list_item_1));

//récuperation des champs du form
        textDateDepart = (TextView) root.findViewById(R.id.textDateDepart);
        dateDepart = (DatePicker) root.findViewById(R.id.dateDepart);
        btnDateDepart = (Button) root.findViewById(R.id.btnDateDepart);

        textHeureDepart = (TextView) root.findViewById(R.id.textHeureDepart);
        heureDepart = (TimePicker) root.findViewById(R.id.heureDepart);
        heureDepart.setIs24HourView(true);
        btnHeureDepart = (Button) root.findViewById(R.id.btnHeureDepart);


        places = (EditText) root.findViewById(R.id.places);

        retard = (EditText) root.findViewById(R.id.retard);

        vehicle = (Spinner) root.findViewById(R.id.vehicle);

        prix = (EditText) root.findViewById(R.id.prix);
        prixLayout = root.findViewById(R.id.prixLayout);
        prixLayout.setVisibility(View.GONE);

        autoroute = (CheckBox) root.findViewById(R.id.autoroute);

        ic_autoroute = (ImageView) root.findViewById(R.id.ic_autoroute);
        ajout = (LinearLayout) root.findViewById(R.id.input_group);
        info = (LinearLayout) root.findViewById(R.id.info);
        tDuree = (TextView) root.findViewById(R.id.duration);
        tDist = (TextView) root.findViewById(R.id.dist);

// listener du bouton pour la date
        btnDateDepart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textDateDepart.setText(dateDepart.getDayOfMonth() + "/" + (dateDepart.getMonth() + 1) + "/" + dateDepart.getYear());
                if (dateDepart.getVisibility() == View.VISIBLE) {
                    dateDepart.setVisibility(View.GONE);
                    btnDateDepart.setText("Date de départ");

                } else {
                    dateDepart.setVisibility(View.VISIBLE);
                    btnDateDepart.setText("Ok");
                }
            }
        });

//listener du bouton pour l'heure
        btnHeureDepart.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                textHeureDepart.setText(heureDepart.getHour() + "h" + heureDepart.getMinute());
                if (heureDepart.getVisibility() == View.VISIBLE) {
                    heureDepart.setVisibility(View.GONE);
                    btnHeureDepart.setText("Heure de départ");

                } else {
                    heureDepart.setVisibility(View.VISIBLE);
                    btnHeureDepart.setText("Ok");
                }
            }
        });

//listener du select vehicule
        vehicle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (String.valueOf(vehicle.getSelectedItem()).equals("En voiture")) {
                    prixLayout.setVisibility(View.VISIBLE);
                    autoroute.setVisibility(View.VISIBLE);
                    ic_autoroute.setVisibility(View.VISIBLE);

                } else {
                    prixLayout.setVisibility(View.GONE);
                    autoroute.setVisibility(View.GONE);
                    ic_autoroute.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                prixLayout.setVisibility(View.GONE);
                autoroute.setVisibility(View.GONE);
                ic_autoroute.setVisibility(View.GONE);
            }

        });

        //listener du bouton ajouter trajet
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //recuperation des informations
                AutoCompleteTextView depart =root.findViewById(R.id.autocomplete);
                DatePicker dateDepart = (DatePicker) root.findViewById(R.id.dateDepart);
                TimePicker heureDepart = (TimePicker) root.findViewById(R.id.heureDepart);
                EditText places = (EditText) root.findViewById(R.id.places);
                EditText retard = (EditText) root.findViewById(R.id.retard);
                Spinner vehicle = (Spinner) root.findViewById(R.id.vehicle);
                CheckBox autoroute = (CheckBox) root.findViewById(R.id.autoroute);
                EditText prix = (EditText) root.findViewById(R.id.prix);

                String start = depart.getText().toString().trim();


                Calendar dDepart = Calendar.getInstance();
                dDepart.set(Calendar.YEAR, dateDepart.getYear());
                dDepart.set(Calendar.MONTH, dateDepart.getMonth());
                dDepart.set(Calendar.DATE, dateDepart.getDayOfMonth());
                dDepart.set(Calendar.HOUR_OF_DAY, heureDepart.getHour()-1);
                dDepart.set(Calendar.MINUTE, heureDepart.getMinute());
                dDepart.set(Calendar.SECOND, 0);


                // Integer.parseInt();
                String nbPlaces = places.getText().toString().trim();
                String minRetard = retard.getText().toString().trim();

                String vehicule = vehicle.getSelectedItem().toString().trim();
                String price = prix.getText().toString().trim();
                int cost=0;
                if (TextUtils.isEmpty(price)) {
                    cost = 0;
                }
                else {
                    cost = Integer.parseInt(prix.getText().toString().trim());
                }


                boolean isVoiture;
                if (vehicule.equals("En voiture")) {
                    isVoiture = true;
                } else {
                    isVoiture = false;
                }

                boolean isAutoroute;
                if (autoroute.isChecked()) {
                    isAutoroute = true;
                } else {
                    isAutoroute = false;
                }

                System.out.println(cost);

// verification des champs
                if (TextUtils.isEmpty(start)) {
                    Toast.makeText(getActivity(), "Entrez un point de départ", Toast.LENGTH_SHORT).show();
                    return;
                }


                if (TextUtils.isEmpty(nbPlaces)) {
                    Toast.makeText(getActivity(), "Entrez le nombre de places disponibles", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(minRetard)) {
                    Toast.makeText(getActivity(), "Entrez le retard toléré", Toast.LENGTH_SHORT).show();
                    return;
                }


                // recuperation de la distance et la durée avec l'API google
                String myURL = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins="+start+"&destinations=ESIGELEC,%20Avenue%20Galil%C3%A9e,%20Saint-%C3%89tienne-du-Rouvray,%20France,NY&key=AIzaSyBERFhpnWbFCaQaWYR5Hvu1sVJBVZdNknc";
                String distance = null;
                String duration = null;
                int secondes = 0;

                try {
                    AsyncTask task = new JsonTask().execute(myURL);
                    Object resulttask = task.get();
                    String json = resulttask.toString();
                    JSONObject jsonObject = new JSONObject(json);
                    JSONArray jsonObject1 = (JSONArray) jsonObject.get("rows");
                    JSONObject jsonObject2 = (JSONObject) jsonObject1.get(0);
                    JSONArray jsonObject3 = (JSONArray) jsonObject2.get("elements");
                    JSONObject elementObj = (JSONObject) jsonObject3.get(0);
                    JSONObject distanceObj = (JSONObject) elementObj.get("distance");
                    JSONObject durationObj = (JSONObject) elementObj.get("duration");
                    distance = distanceObj.getString("text");
                    duration = durationObj.getString("text");
                    secondes = durationObj.getInt("value");

                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                FirebaseUser user = mAuth.getCurrentUser();
                createTrajet(user.getUid(), dDepart.getTime(), Integer.parseInt(minRetard), Integer.parseInt(nbPlaces), vehicle.getSelectedItem().toString().trim() ,isVoiture, isAutoroute, cost,duration,secondes,distance, start,"ESIGELEC, Avenue Galilée, Saint-Étienne-du-Rouvray, France");
                ajout.setVisibility(View.GONE);
                info.setVisibility(View.VISIBLE);
                tDist.setText(distance);
                tDuree.setText(duration);
                Toast.makeText(getActivity(),"Votre trajet a bien été ajouté. Vous le retrouverez dans la rubrique Mes Trajets",Toast.LENGTH_SHORT).show();
            }
        });
        return root;
    }
//ajout a firebase
    public void createTrajet(String createurId, Date depart, int retard, int nbrPlaces, String vehicule, boolean isVoitue, boolean isAutoroute, int prix, String duration,int secondes,String distance, String adresseDepart , String addresseArrivee) {
        Log.d(TAG, mAuth.getCurrentUser().getUid());
        Trajet trajet = new Trajet(createurId, depart, retard, nbrPlaces, vehicule, isVoitue, isAutoroute, prix, duration, secondes,distance,adresseDepart, addresseArrivee);

        db.collection("trajets")
                .add(trajet)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                    }
                });
    }

//methode permettant de recuperer le JSON de googlz
    private class JsonTask extends AsyncTask<String, String, String> {
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line+"\n");
                   // Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)

                }
                return buffer.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            //txtJson.setText(result);
        }
    }



}