package com.source.gotoesig.ui.trajets;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.source.gotoesig.R;
import com.source.model.Trajet;

import java.util.ArrayList;
import java.util.List;

public class TrajetsAdapter extends RecyclerView.Adapter<TrajetsViewHolder>{

    List<Trajet> list;
    private Context mContext;

    //ajouter un constructeur prenant en entrée une liste
    public TrajetsAdapter(Context context, List<Trajet> list) {
        this.list = list;
        mContext = context;
    }

    //cette fonction permet de créer les viewHolder
    //et par la même indiquer la vue à inflater (à partir des layout xml)
    @Override
    public TrajetsViewHolder onCreateViewHolder(ViewGroup viewGroup, int itemType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_mes_trajets,viewGroup,false);
        return new TrajetsViewHolder(view);
    }

    //c'est ici que nous allons remplir notre cellule avec le texte/image de chaque MyObjects
    @Override
    public void onBindViewHolder(TrajetsViewHolder trajetsViewHolder, final int position) {
        Trajet myObject = list.get(position);
        trajetsViewHolder.bind(myObject);
        final ArrayList<String> str = new ArrayList<String>();
        for(Trajet res : list) {
            str.add(res.getId());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        RelativeLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            parentLayout = itemView.findViewById(R.id.parent_layoutMesTrajets);
        }
    }

}
