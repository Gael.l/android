package com.source.gotoesig.ui.trajets;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.source.gotoesig.R;
import com.source.model.Trajet;
import com.source.model.User;

import java.util.Calendar;
import java.util.Date;

public class TrajetsViewHolder extends RecyclerView.ViewHolder {

    TextView textViewView;
    TextView dateDeDepartMesTrajets;
    TextView adresseDepartMesTrajets;
    TextView adresseArriveeMesTrajets;
    TextView conducteur;
    TextView heureDeDepartMesTrajets;
    TextView heureDarriveeMesTrajets;
    private FirebaseFirestore db;
    private static final String TAG = "TrajetViewHolder";
    //itemView est la vue correspondante à 1a cellule
    public TrajetsViewHolder(View itemView) {
        super(itemView);
        //c'est ici que l'on fait nos findView
        textViewView = (TextView) itemView.findViewById(R.id.text);
        dateDeDepartMesTrajets = itemView.findViewById(R.id.dateDeDepartMesTrajets);
        adresseDepartMesTrajets = itemView.findViewById(R.id.adresseDepartMesTrajets);
        adresseArriveeMesTrajets = itemView.findViewById(R.id.adresseArriveeMesTrajets);
        conducteur = itemView.findViewById(R.id.conducteur);
        heureDeDepartMesTrajets = itemView.findViewById(R.id.heureDeDepartMesTrajets);
        heureDarriveeMesTrajets = itemView.findViewById(R.id.heureDarriveeMesTrajets);
    }

    public void bind(Trajet trajet){
        db = FirebaseFirestore.getInstance();

        Date dateDujour = new Date();
        Date dateDepart = trajet.getDepart();

        /** Date de depart */
        Calendar calendarDepart = Calendar.getInstance();
        calendarDepart.setTime(dateDepart);

        /** Date d'arrivée */
        Calendar calendarArrivee = Calendar.getInstance();
        calendarArrivee.setTime(dateDepart);
        calendarArrivee.add(Calendar.SECOND, trajet.getSecondes());

        /** Date du jour*/
        Calendar calendarDateDuJour = Calendar.getInstance();
        calendarDateDuJour.setTime(dateDujour);

        int day = calendarDepart.get(Calendar.DATE);
        int month = calendarDepart.get(Calendar.MONTH) + 1;
        int year = calendarDepart.get(Calendar.YEAR);
        int dow = calendarDepart.get(Calendar.DAY_OF_WEEK);
        int dom = calendarDepart.get(Calendar.DAY_OF_MONTH);
        int doy = calendarDepart.get(Calendar.DAY_OF_YEAR);

        String jour=null;
        String mois=null;
        if(dow==1){
            jour = "Dimanche";
        }

        if(dow==2){
            jour = "Lundi";
        }

        if(dow==3){
            jour = "Mardi";
        }

        if(dow==4){
            jour = "Mercredi";
        }

        if(dow==5){
            jour = "Jeudi";
        }

        if(dow==6){
            jour = "Vendredi";
        }

        if(dow==7){
            jour = "Samedi";
        }

        if(month==1){
            mois = "Janvier";

        }
        if(month==2){
            mois = "Février";

        }
        if(month==3){
            mois = "Mars";

        }
        if(month==4){
            mois = "Avril";

        }
        if(month==5){
            mois = "Mai";

        }
        if(month==6){
            mois = "Juin";

        }
        if(month==7){
            mois = "Juillet";

        }
        if(month==8){
            mois = "Aout";

        }
        if(month==9){
            mois = "Septembre";

        }
        if(month==10){
            mois = "Octobre";

        }
        if(month==11){
            mois = "Novembre";

        }

        if(month==12){
            mois = "Décembre";

        }
        int hour = calendarDepart.get(Calendar.HOUR_OF_DAY)+1;
        int minute = calendarDepart.get(Calendar.MINUTE);

        int hArrive = calendarArrivee.get(Calendar.HOUR_OF_DAY)+1;
        int mArrive = calendarArrivee.get(Calendar.MINUTE);

        String statut = "";
        if(calendarDateDuJour.before(calendarDepart)) {
            statut = "Trajet à venir le " + dom + " " + mois;
            Log.d(TAG, "bind: trajet a venir : " + trajet.getRetard());
        } else if (calendarDateDuJour.after(calendarArrivee)) {
            statut = "Trajet terminé le " + dom + " " + mois;
        } else if(calendarDateDuJour.after(calendarDepart) && calendarDateDuJour.before(calendarArrivee)) {
            statut = "Trajet en cours";
            Log.d(TAG, "bind: Trajet en cours : " + trajet.getRetard());
        }
        String zero ="";
        String Arriveezero ="";
        if(minute < 10 ) {
            zero="0";
        } else {
            zero="";
        }

        if(mArrive < 10 ) {
            Arriveezero="0";
        } else {
            Arriveezero="";
        }
        dateDeDepartMesTrajets.setText(statut);
        adresseDepartMesTrajets.setText(trajet.getAdresseDepart());
        adresseArriveeMesTrajets.setText(trajet.getAdresseArivee());
        heureDeDepartMesTrajets.setText(hour+"h"+ zero+minute);
        heureDarriveeMesTrajets.setText(hArrive+"h"+Arriveezero+mArrive);

        DocumentReference userRef = db.collection("users").document(trajet.getCreateurId());
        userRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                conducteur.setText( "Conducteur : " + documentSnapshot.toObject(User.class).getFirstName());
            }
        });

    }
}
