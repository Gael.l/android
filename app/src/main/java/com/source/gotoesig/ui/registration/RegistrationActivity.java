package com.source.gotoesig.ui.registration;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.source.gotoesig.R;
import com.source.gotoesig.ui.login.LoginActivity;
import com.source.model.Evaluation;
import com.source.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static android.Manifest.permission.READ_PHONE_NUMBERS;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.READ_SMS;


public class RegistrationActivity extends AppCompatActivity implements LocationListener {
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private static final int PERMISSION_GPS = 100;
    private LocationManager lm;
    private Double longitude;
    private Double latitude;



    private static final String TAG = "RegistrationActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_GPS);
        } else {
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,RegistrationActivity.this);
        }

        TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, READ_SMS) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{READ_SMS, READ_PHONE_NUMBERS, READ_PHONE_STATE}, 100);
        }
        String mPhoneNumber = tMgr.getLine1Number();



        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        Button signIn_btn = findViewById(R.id.signIn_btn);
        TextView signIn_Text = findViewById(R.id.signIn_text);

        signIn_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //récuperation des information


                EditText mFirstName = findViewById(R.id.firstName);
                EditText mLastName = findViewById(R.id.lastName);
                EditText mEmail = findViewById(R.id.email);
                EditText mPassword = findViewById(R.id.password);
                EditText mPasswordConfirm = findViewById(R.id.passwordConfirm);

                String firstName = mFirstName.getText().toString().trim();
                String lastName = mLastName.getText().toString().trim();
                String phone = mPhoneNumber;
                String email = mEmail.getText().toString().trim();
                String password = mPassword.getText().toString().trim();
                String passwordConfirm = mPasswordConfirm.getText().toString().trim();


               //obtention de la position de smartphone avec l'api google

                String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latitude+","+longitude+"&key=AIzaSyBERFhpnWbFCaQaWYR5Hvu1sVJBVZdNknc";
                String city = null;
                try {

                    AsyncTask task = new JsonTask().execute(url);
                    Object resulttask = task.get();
                    String json = resulttask.toString();
                    JSONObject jsonObject = new JSONObject(json);
                    JSONArray jsonObject1 = (JSONArray) jsonObject.get("results");
                    JSONObject jsonObject2 = (JSONObject) jsonObject1.get(0);
                    JSONArray jsonObject3 = (JSONArray) jsonObject2.get("address_components");
                    JSONObject jsonObj = (JSONObject) jsonObject3.get(2);
                    city = jsonObj.getString("long_name");
                    System.out.println(city);

                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


//vérification des champs
                if (TextUtils.isEmpty(firstName)) {
                    Toast.makeText(getApplicationContext(), "Entrez un prénom", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(lastName)) {
                    Toast.makeText(getApplicationContext(), "Entrez un nom", Toast.LENGTH_SHORT).show();
                    return;
                }


                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Entrez une adresse mail", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Entrez un mot de passe", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(passwordConfirm)) {
                    Toast.makeText(getApplicationContext(), "Confirmez le mot de passe", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (password.length() < 6) {
                    Toast.makeText(getApplicationContext(), "Mot de passe trop court (min 6 caractères)", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!password.equals(passwordConfirm)) {
                    Toast.makeText(getApplicationContext(), "Les mots de passe ne correspondent pas", Toast.LENGTH_SHORT).show();
                    return;
                }

                createAccount(firstName, lastName, email, city, phone, password);




            }
        });
        //retour a la page de connexion
        signIn_Text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegistrationActivity.this, LoginActivity.class));
                finish();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    //Change UI according to user data.
    public void updateUI(FirebaseUser account) {
        if (account != null) {
            Toast.makeText(this, "Connexion réussie", Toast.LENGTH_LONG).show();
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

    public void createAccount(String firstName, String lastName, String email, String ville, String phone, String password) {

        final String fName = firstName;
        final String lName = lastName;
        final String city = ville;
        final String tel = phone;
        final String mail = email;


        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Log.d(TAG, "dans le if     :   " + user.getUid());
                            updateUI(user);
                            createUser(user.getUid(), fName, lName, mail, city, tel);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(RegistrationActivity.this, "Connexion échouée",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                        Log.d(TAG, "eezgergre   " + mAuth.getCurrentUser().getUid());

                        // ...
                    }
                });
    }

    public void createUser(String id, String firstName, String lastName, String email, String ville, String phone) {
        Log.d(TAG, mAuth.getCurrentUser().getUid());

        String url = "https://eu.ui-avatars.com/api/?name=" + firstName + "+" + lastName + "&size=500&rounded=true&bold=true&background=ffffff&color=3377e8";
        Evaluation eval = new Evaluation("", "", 0);
        ArrayList<Evaluation> evalList = new ArrayList<>();
        evalList.add(eval);
        User user = new User(id, firstName, lastName, email, ville, phone, 0, url, "", evalList);

        db.collection("users").document(id)
                .set(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully written!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                    }
                });
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null){
            Log.d(TAG, "onLocationChanged: ******");
            longitude = location.getLongitude();
            latitude = location.getLatitude();
            Log.d(TAG, "onLocationChanged: **** " + latitude);
        }
    }

    @Override
    public void onRequestPermissionsResult (int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        if (requestCode == PERMISSION_GPS){
            if(ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,RegistrationActivity.this);
                else
                    Toast.makeText(RegistrationActivity.this,"Permission refusée ! ",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


    private class JsonTask extends AsyncTask<String, String, String> {
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line+"\n");
                    // Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)

                }
                return buffer.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            //txtJson.setText(result);
        }
    }
    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{READ_SMS, READ_PHONE_NUMBERS, READ_PHONE_STATE}, 100);
        }
    }

}
