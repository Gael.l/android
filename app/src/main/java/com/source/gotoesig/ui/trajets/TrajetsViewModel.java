package com.source.gotoesig.ui.trajets;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class TrajetsViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public TrajetsViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Vos trajets proposés");
    }

    public LiveData<String> getText() {
        return mText;
    }
}