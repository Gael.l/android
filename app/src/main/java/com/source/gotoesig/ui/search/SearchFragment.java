package com.source.gotoesig.ui.search;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.source.gotoesig.R;
import com.source.model.Trajet;

import java.util.ArrayList;
import java.util.List;

public class SearchFragment extends Fragment {

    private SearchViewModel searchViewModel;
    private List<Trajet> trajetsFb = new ArrayList<>();
    private FirebaseFirestore db;
    private FirebaseAuth mauth;
    private static final String TAG = "SearchFragment";
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        searchViewModel = ViewModelProviders.of(this).get(SearchViewModel.class);
        View root = inflater.inflate(R.layout.fragment_search, container, false);
        final TextView textView = root.findViewById(R.id.text_search);
        final ImageView img = root.findViewById(R.id.img);
        final TextView txt = root.findViewById(R.id.not_foundbb);
        final RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        searchViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                db = FirebaseFirestore.getInstance();
                mauth = FirebaseAuth.getInstance();
                db.collection("trajets")
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        Log.d(TAG, document.getId() + " => " + document.getData());

                                        /** On convertit le document en objet Java Trajet */
                                        Trajet trajet = document.toObject(Trajet.class);
                                        trajet.setId(document.getId());

                                        /** Créations des booleans permettant de verifier la présence d'un trajet dans la liste des trajets a chercher*/
                                        boolean isCurrentUserParticipant =  trajet.getParticipants() != null && trajet.getParticipants().stream().anyMatch(id -> id.equals(mauth.getUid()));
                                        boolean isParticipantsListNull = trajet.getParticipants() == null;
                                        boolean isCurrentUserCreator = trajet.getCreateurId().equals(mauth.getUid());
                                        boolean isPlacesGreaterThanNull = trajet.getNbrPlaces() > 0;

                                        /** Formulation de la condition */
                                        if(isPlacesGreaterThanNull && !isCurrentUserCreator && (!isCurrentUserParticipant || isParticipantsListNull)) {
                                            trajetsFb.add(trajet);
                                        }
                                    }
                                } else {
                                    Log.d(TAG, "Error getting documents: ", task.getException());
                                }
                                if(trajetsFb.isEmpty()) {
                                    txt.setVisibility(View.VISIBLE);
                                    img.setVisibility(View.VISIBLE);
                                } else {
                                    txt.setVisibility(View.GONE);
                                    img.setVisibility(View.GONE);
                                }
                                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                                recyclerView.setAdapter(new SearchAdapter(getContext(), trajetsFb));
                            }
                        });
                textView.setText(s);
            }
        });
        return root;
    }
}