package com.source.gotoesig.ui.leave;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.firebase.auth.FirebaseAuth;
import com.source.gotoesig.ui.login.LoginActivity;
import com.source.gotoesig.R;

public class LeaveFragment extends Fragment {

    private LeaveViewModel leaveViewModel;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(getActivity(), LoginActivity.class));

        leaveViewModel =
                ViewModelProviders.of(this).get(LeaveViewModel.class);
        View root = inflater.inflate(R.layout.fragment_leave, container, false);
        final TextView textView = root.findViewById(R.id.text_leave);
        leaveViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;

    }
}