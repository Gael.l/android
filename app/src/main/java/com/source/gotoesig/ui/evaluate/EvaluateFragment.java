package com.source.gotoesig.ui.evaluate;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.source.gotoesig.R;
import com.source.gotoesig.ui.trajets.TrajetsViewModel;
import com.source.model.Trajet;
import com.source.model.User;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class EvaluateFragment extends Fragment {
    private TrajetsViewModel trajetsViewModel;
    private List<Trajet> trajets = new ArrayList<>();
    private FirebaseFirestore db;
    private FirebaseAuth mauth;
    private static final String TAG = "EvaluateFragment";

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        trajetsViewModel = ViewModelProviders.of(this).get(TrajetsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_evaluate, container, false);
        final ImageView img = root.findViewById(R.id.img);
        final TextView txt = root.findViewById(R.id.not_foundbb);
        final TextView textView = root.findViewById(R.id.text_search);
        final RecyclerView recyclerView = root.findViewById(R.id.recyclerView);

        trajetsViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                db = FirebaseFirestore.getInstance();
                mauth = FirebaseAuth.getInstance();

                /** On recupere les trajets à afficher */
                db.collection("trajets")
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        Log.d(TAG, document.getId() + " => " + document.getData());

                                        /** On convertit le document en objet Java Trajet */
                                        Trajet trajet = document.toObject(Trajet.class);
                                        trajet.setId(document.getId());

                                        /** On récupère la personne qui a crée le trajet afin de connaitre les personnes ayant deja evalué son trajet */
                                        DocumentReference userRef = db.collection("users").document(trajet.getCreateurId());
                                        userRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                           @Override
                                           public void onSuccess(DocumentSnapshot documentSnapshot) {
                                               User creatorUser = documentSnapshot.toObject(User.class);

                                               Log.d(TAG, "isCurrentUserParticipant  : " + isCurrentUserParticipant(trajet));
                                               Log.d(TAG, "hasNeverEvaluated         : " + hasNeverEvaluated(creatorUser));
                                               Log.d(TAG, "isTrajetFinished          : " + isTrajetFinished(trajet.getDepart(), trajet.getSecondes()));

                                               if(isCurrentUserParticipant(trajet) && hasNeverEvaluated(creatorUser) && isTrajetFinished(trajet.getDepart(), trajet.getSecondes())) {
                                                   trajets.add(trajet);
                                               }
                                               if(trajets.isEmpty()) {
                                                   txt.setVisibility(View.VISIBLE);
                                                   img.setVisibility(View.VISIBLE);
                                               } else {
                                                   txt.setVisibility(View.GONE);
                                                   img.setVisibility(View.GONE);
                                               }
                                               recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                                               recyclerView.setAdapter(new EvaluateAdapter(getContext(), trajets));
                                           }
                                       });
                                    }
                                } else {
                                    Log.d(TAG, "Error getting documents: ", task.getException());
                                }
                            }
                        });
                textView.setText(s);
            }
        });
        return root;
    }


    /**
     * @return boolean
     * @description Permet de savoir si le trajet est terminé
     * */
    public boolean isTrajetFinished(Date dateDePart, int dureeTrajet) {
        boolean res = false;
        Date dateDuJour = new Date();

        /** Date de depart */
        Calendar calendarDepart = Calendar.getInstance();
        calendarDepart.setTime(dateDePart);

        /** Date d'arrivée */
        Calendar calendarArrivee = Calendar.getInstance();
        calendarArrivee.setTime(dateDePart);
        calendarArrivee.add(Calendar.SECOND, dureeTrajet);

        /** Date du jour*/
        Calendar calendarDateDuJour = Calendar.getInstance();
        calendarDateDuJour.setTime(dateDuJour);

        if(calendarDateDuJour.after(calendarArrivee)) {
            res = true;
        }
        return res;
    }

    /**
     * @return boolean
     * @description Permet de savoir si l'utilisateur connecté a déjà evalué le trajet, renvoie true si il n'a jamais evalué
     * */
    public boolean hasNeverEvaluated(User creatorUser) {
        return creatorUser.getUserTrajetEvaluate().stream().noneMatch(id -> id.getIdUser().equals(mauth.getUid()));
    }

    /**
     * @return boolean
     * @description Permet de savoir si l'utilisateur connecté participe au trajet
     * */
    public boolean isCurrentUserParticipant(Trajet trajet) {
        return trajet.getParticipants() != null && trajet.getParticipants().stream().anyMatch(id -> id.equals(mauth.getUid()));
    }

}