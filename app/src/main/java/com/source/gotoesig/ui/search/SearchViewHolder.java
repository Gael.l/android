package com.source.gotoesig.ui.search;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.source.gotoesig.R;
import com.source.model.Trajet;

import java.util.Calendar;
import java.util.Date;

public class SearchViewHolder extends RecyclerView.ViewHolder{

    TextView textViewView;
    TextView textView;
    TextView dateDeDepart;
    TextView prix;
    TextView places;
    TextView MoyenTransport;
    TextView adresseDepart;
    TextView adresseArrivee;
    TextView retard;
    TextView heureDepart;
    TextView heureArrivee;
    private static final String TAG = "SearchViewHolder";
    //itemView est la vue correspondante à 1a cellule
    public SearchViewHolder(View itemView) {
        super(itemView);
        //c'est ici que l'on fait nos findView
        textViewView = (TextView) itemView.findViewById(R.id.text);
        dateDeDepart = itemView.findViewById(R.id.dateDeDepart);
        prix = itemView.findViewById(R.id.prix);
        places = itemView.findViewById(R.id.places);
        MoyenTransport = itemView.findViewById(R.id.MoyenTransport);
        adresseDepart = itemView.findViewById(R.id.adresseDepart);
        adresseArrivee = itemView.findViewById(R.id.adresseArrivee);
        retard = itemView.findViewById(R.id.retard);
        heureDepart = itemView.findViewById(R.id.heureDeDepart);
        heureArrivee = itemView.findViewById(R.id.heureDarrivee);
    }

    public void bind(Trajet trajet){
        // textViewView.setText(myObject.getText());
        Log.d(TAG, "Date de depart  :   " + trajet.getDepart());

        Date date = trajet.getDepart();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date);
        int secondes = trajet.getSecondes();
        cal2.add(Calendar.SECOND,secondes);
        int day = calendar.get(Calendar.DATE);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);
        int dow = calendar.get(Calendar.DAY_OF_WEEK);
        int dom = calendar.get(Calendar.DAY_OF_MONTH);
        int doy = calendar.get(Calendar.DAY_OF_YEAR);


        String jour=null;
        String mois=null;
        if(dow==1){
            jour = "Dimanche";
        }

        if(dow==2){
            jour = "Lundi";
        }

        if(dow==3){
            jour = "Mardi";
        }

        if(dow==4){
            jour = "Mercredi";
        }

        if(dow==5){
            jour = "Jeudi";
        }

        if(dow==6){
            jour = "Vendredi";
        }

        if(dow==7){
            jour = "Samedi";
        }

        if(month==1){
            mois = "Janvier";

        }
        if(month==2){
            mois = "Février";

        }
        if(month==3){
            mois = "Mars";

        }
        if(month==4){
            mois = "Avril";

        }
        if(month==5){
            mois = "Mai";

        }
        if(month==6){
            mois = "Juin";

        }
        if(month==7){
            mois = "Juillet";

        }
        if(month==8){
            mois = "Aout";

        }
        if(month==9){
            mois = "Septembre";

        }
        if(month==10){
            mois = "Octobre";

        }
        if(month==11){
            mois = "Novembre";

        }

        if(month==12){
            mois = "Décembre";

        }



        int hour = calendar.get(Calendar.HOUR_OF_DAY)+1;
        int minute = calendar.get(Calendar.MINUTE);

        int hArrive = cal2.get(Calendar.HOUR_OF_DAY)+1;
        int mArrive = cal2.get(Calendar.MINUTE);

        String zero ="";
        String Arriveezero ="";
        if(minute < 10 ) {
            zero="0";
        } else {
            zero="";
        }

        if(mArrive < 10 ) {
            Arriveezero="0";
        } else {
            Arriveezero="";
        }

        dateDeDepart.setText(jour+" "+day+" "+mois+" "+year+" à "+hour+"h"+zero+minute);
        prix.setText(Integer.toString(trajet.getPrix()) + " €");
        places.setText(Integer.toString(trajet.getNbrPlaces()));
        MoyenTransport.setText(trajet.getVehicule());
        adresseDepart.setText(trajet.getAdresseDepart());
        adresseArrivee.setText(trajet.getAdresseArivee());
        retard.setText( "Retard toléré : " + Integer.toString(trajet.getRetard()) + " min");
        heureDepart.setText(hour+"h"+zero+minute);
        heureArrivee.setText(hArrive+"h"+Arriveezero+mArrive);
    }
}
