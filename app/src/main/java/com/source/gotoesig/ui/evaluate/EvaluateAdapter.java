package com.source.gotoesig.ui.evaluate;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.source.gotoesig.R;
import com.source.model.Trajet;
import com.source.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EvaluateAdapter  extends RecyclerView.Adapter<EvaluateViewHolder>{

    List<Trajet> list;
    private Context mContext;
    private RecyclerView.Adapter mAdapter;
    private static final String TAG = "EvaluateAdapter";
    private FirebaseFirestore db;
    private FirebaseAuth mauth;

    //ajouter un constructeur prenant en entrée une liste
    public EvaluateAdapter(Context context, List<Trajet> list) {
        this.list = list;
        mContext = context;
    }



    //cette fonction permet de créer les viewHolder
    //et par la même indiquer la vue à inflater (à partir des layout xml)
    @Override
    public EvaluateViewHolder onCreateViewHolder(ViewGroup viewGroup, int itemType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_evaluer_trajet,viewGroup,false);
        return new EvaluateViewHolder(view, list);
    }



    //c'est ici que nous allons remplir notre cellule avec le texte/image de chaque MyObjects
    @Override
    public void onBindViewHolder(EvaluateViewHolder evaluateViewHolder, final int position) {

        Trajet trajet = list.get(position);
        evaluateViewHolder.bind(trajet);

        /** Lors du clique sur valider un score */
        evaluateViewHolder.btnValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db = FirebaseFirestore.getInstance();
                mauth = FirebaseAuth.getInstance();
                DocumentReference userRef = db.collection("users").document(trajet.getCreateurId());
                float score = evaluateViewHolder.scoreEvaluate.getRating();

                /** On insere en base dans la table User, dans la liste des Evaluations, l'id du trajet, l'id du User qui vient de noter et le score donné*/
                Map<String, Object> eval = new HashMap<>();
                eval.put("idTrajet", trajet.getId());
                eval.put("idUser", mauth.getUid());
                eval.put("score", score);

                /** Inseration de l'eval en base */
                userRef.update("userTrajetEvaluate", FieldValue.arrayUnion(eval));

                /** Calcul de la moyenne des scores + inserer la moyenne en base dans le champ score*/
                userRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        User user = documentSnapshot.toObject(User.class);
                        ArrayList<Integer> scores = new ArrayList<>();
                        float moyenne = 0;
                        float total = 0;
                        scores = user.getUserTrajetEvaluate().stream().map(eval -> eval.getScore()).collect(Collectors.toCollection(ArrayList::new));
                        for(float note : scores) {
                            total += note;
                        }
                        moyenne = total / scores.size();
                        userRef.update("score", moyenne);
                        Toast.makeText(mContext, "Merci d'avoir noté ce trajet !", Toast.LENGTH_LONG).show();
                        removeItem(position);
                    }
                });
            }
        });
        final ArrayList<String> str = new ArrayList<String>();
        for(Trajet res : list) {
            str.add(res.getId());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void removeItem(int position) {
        this.list.remove(position);
        Log.d(TAG, "removeItem: " + list);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getItemCount());
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        RelativeLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            parentLayout = itemView.findViewById(R.id.parent_layoutEvaluer);
        }
    }
}
