package com.source.gotoesig.ui.stats;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.source.gotoesig.R;
import com.source.model.Trajet;
import com.source.model.User;

public class StatsFragment extends Fragment {

    private StatsViewModel statsViewModel;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private static final String TAG = "StatsFragment";

    private TextView nbrTrajetPropose;
    private TextView nbrTrajetEffectue;
    private TextView scoreMoy;
    private TextView nbrAvis;
    private TextView montant;
    private int nbrTrajetProposes = 0;
    private int nbrTrajetEffectues = 0;
    private int montantTotal = 0;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        statsViewModel = ViewModelProviders.of(this).get(StatsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_stats, container, false);

        final TextView textView = root.findViewById(R.id.text_stats);
        final TextView nbrTrajetPropose = root.findViewById(R.id.nbrTrajetPropose);
        final TextView nbrTrajetEffectue = root.findViewById(R.id.nbrTrajetEffectue);
        final TextView scoreMoy = root.findViewById(R.id.scoreMoy);
        final TextView nbrAvis = root.findViewById(R.id.nbrAvis);
        final TextView montant = root.findViewById(R.id.montant);

        statsViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                mAuth = FirebaseAuth.getInstance();
                db = FirebaseFirestore.getInstance();
                textView.setText(s);
                db.collection("trajets")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());

                                /** On convertit le document en objet Java Trajet */
                                Trajet trajet = document.toObject(Trajet.class);
                                trajet.setId(document.getId());

                                if(trajet.getCreateurId().equals(mAuth.getUid())) {
                                    nbrTrajetProposes +=1;
                                    int montant = trajet.getParticipants().size() * trajet.getPrix();
                                    montantTotal = montantTotal + montant;
                                }

                                if(trajet.getParticipants().stream().anyMatch(participantId -> participantId.equals(mAuth.getUid()))) {
                                    nbrTrajetEffectues +=1;
                                }
                                DocumentReference docRef = db.collection("users").document(mAuth.getUid());
                                docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        // Transforme data en objet User
                                        User user = documentSnapshot.toObject(User.class);
                                        int nbrAviss = user.getUserTrajetEvaluate().size() - 1 ;
                                        float noteMoye = user.getScore();

                                        nbrTrajetPropose.setText(String.valueOf(nbrTrajetProposes) + " Trajets proposés");
                                        nbrTrajetEffectue.setText(String.valueOf(nbrTrajetEffectues) + " Réservations" );
                                        scoreMoy.setText(String.valueOf(user.getScore()) + "/5");
                                        montant.setText(String.valueOf(montantTotal) + " euros");
                                        nbrAvis.setText(String.valueOf(nbrAviss) +  " avis");
                                    }
                                });

                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
            }
        });
        return root;
    }
}