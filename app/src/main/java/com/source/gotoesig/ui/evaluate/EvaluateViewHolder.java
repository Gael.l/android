package com.source.gotoesig.ui.evaluate;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.source.gotoesig.R;
import com.source.model.Trajet;
import com.source.model.User;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EvaluateViewHolder extends RecyclerView.ViewHolder {

    TextView textViewView;
    TextView dateDeDepartEvaluer;
    TextView adresseDepartEvaluer;
    TextView adresseArriveeEvaluer;
    TextView conducteurEvaluer;
    TextView heureDeDepartEvaluer;
    TextView heureDarriveeEvaluer;
    RatingBar scoreEvaluate;
    Button btnValider;
    List<Trajet> list;
    private boolean change = false;
    private FirebaseFirestore db;
    private FirebaseAuth mauth;
    private static final String TAG = "EvaluateViewHolder";


    //itemView est la vue correspondante à 1a cellule
    public EvaluateViewHolder(View itemView, List<Trajet> list) {
        super(itemView);
        this.list = list;
        //c'est ici que l'on fait nos findView
        textViewView = (TextView) itemView.findViewById(R.id.text);
        dateDeDepartEvaluer= itemView.findViewById(R.id.dateDeDepartEvaluer);
        adresseDepartEvaluer= itemView.findViewById(R.id.adresseDepartEvaluer);
        adresseArriveeEvaluer= itemView.findViewById(R.id.adresseArriveeEvaluer);
        conducteurEvaluer = itemView.findViewById(R.id.conducteurEvaluer);
        heureDeDepartEvaluer= itemView.findViewById(R.id.heureDeDepartEvaluer);
        heureDarriveeEvaluer= itemView.findViewById(R.id.heureDarriveeEvaluer);
        scoreEvaluate = itemView.findViewById((R.id.scoreEvaluate));
        btnValider = (Button) itemView.findViewById(R.id.btnValider);
    }

    public void bind(Trajet trajet){
        db = FirebaseFirestore.getInstance();

        Date dateDujour = new Date();
        Date dateDepart = trajet.getDepart();

        /** Date de depart */
        Calendar calendarDepart = Calendar.getInstance();
        calendarDepart.setTime(dateDepart);

        /** Date d'arrivée */
        Calendar calendarArrivee = Calendar.getInstance();
        calendarArrivee.setTime(dateDepart);
        calendarArrivee.add(Calendar.SECOND, trajet.getSecondes());

        /** Date du jour*/
        Calendar calendarDateDuJour = Calendar.getInstance();
        calendarDateDuJour.setTime(dateDujour);

        int day = calendarDepart.get(Calendar.DATE);
        int month = calendarDepart.get(Calendar.MONTH) + 1;
        int year = calendarDepart.get(Calendar.YEAR);
        int dow = calendarDepart.get(Calendar.DAY_OF_WEEK);
        int dom = calendarDepart.get(Calendar.DAY_OF_MONTH);
        int doy = calendarDepart.get(Calendar.DAY_OF_YEAR);

        String jour=null;
        String mois=null;

        if(dow==1){
            jour = "Dimanche";
        }

        if(dow==2){
            jour = "Lundi";
        }

        if(dow==3){
            jour = "Mardi";
        }

        if(dow==4){
            jour = "Mercredi";
        }

        if(dow==5){
            jour = "Jeudi";
        }

        if(dow==6){
            jour = "Vendredi";
        }

        if(dow==7){
            jour = "Samedi";
        }

        if(month==1){
            mois = "Janvier";

        }
        if(month==2){
            mois = "Février";

        }
        if(month==3){
            mois = "Mars";

        }
        if(month==4){
            mois = "Avril";

        }
        if(month==5){
            mois = "Mai";

        }
        if(month==6){
            mois = "Juin";

        }
        if(month==7){
            mois = "Juillet";

        }
        if(month==8){
            mois = "Aout";

        }
        if(month==9){
            mois = "Septembre";

        }
        if(month==10){
            mois = "Octobre";

        }
        if(month==11){
            mois = "Novembre";

        }

        if(month==12){
            mois = "Décembre";

        }
        int hour = calendarDepart.get(Calendar.HOUR_OF_DAY);
        int minute = calendarDepart.get(Calendar.MINUTE);

        int hArrive = calendarArrivee.get(Calendar.HOUR_OF_DAY);
        int mArrive = calendarArrivee.get(Calendar.MINUTE);

        String statut = "";
        if(calendarDateDuJour.before(calendarDepart)) {
            statut = "Trajet à venir le " + dom + " " + mois;
            Log.d(TAG, "bind: trajet a venir : " + trajet.getRetard());
        } else if (calendarDateDuJour.after(calendarArrivee)) {
            statut = "Trajet terminé le " + dom + " " + mois;
        } else if(calendarDateDuJour.after(calendarDepart) && calendarDateDuJour.before(calendarArrivee)) {
            statut = "Trajet en cours";
            Log.d(TAG, "bind: Trajet en cours : " + trajet.getRetard());
        }
        String zero ="";
        String Arriveezero ="";
        if(minute < 10 ) {
            zero="0";
        } else {
            zero="";
        }

        if(mArrive < 10 ) {
            Arriveezero="0";
        } else {
            Arriveezero="";
        }
        dateDeDepartEvaluer.setText(statut);
        adresseDepartEvaluer.setText(trajet.getAdresseDepart());
        adresseArriveeEvaluer.setText(trajet.getAdresseArivee());
        heureDeDepartEvaluer.setText(hour+"h"+ zero+minute);
        heureDarriveeEvaluer.setText(hArrive+"h"+Arriveezero+mArrive);
        /** On recupère le nom du conducteur */
        DocumentReference userRef = db.collection("users").document(trajet.getCreateurId());
        userRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                conducteurEvaluer.setText( "Conducteur : " + documentSnapshot.toObject(User.class).getFirstName());
            }
        });

        /** Lors du clique sur valider un score */
        btnValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db = FirebaseFirestore.getInstance();
                mauth = FirebaseAuth.getInstance();
                DocumentReference userRef = db.collection("users").document(trajet.getCreateurId());
                int score = scoreEvaluate.getNumStars();

                /** On verifie que le champ n'est pas nul et que le score entré < 5 */
                boolean scoreFieldNotNull = !scoreEvaluate.equals(null) || !scoreEvaluate.equals("");
                boolean scoreUnderFive = score < 5;
                /** On insere en base dans la table User, dans la liste des Evaluations, l'id du trajet, l'id du User qui vient de noter et le score donné*/
                Map<String, Object> eval = new HashMap<>();
                eval.put("idTrajet", trajet.getId());
                eval.put("idUser", mauth.getUid());
                eval.put("score", score);

                /** Inseration de l'eval en base */
                userRef.update("userTrajetEvaluate", FieldValue.arrayUnion(eval));

                /** Calcul de la moyenne des scores + inserer la moyenne en base dans le champ score*/
                userRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        User user = documentSnapshot.toObject(User.class);
                        ArrayList<Integer> scores = new ArrayList<>();
                        float moyenne = 0;
                        float total = 0;
                        scores = user.getUserTrajetEvaluate().stream().map(eval -> eval.getScore()).collect(Collectors.toCollection(ArrayList::new));
                        for(float note : scores) {
                            total += note;
                        }
                        moyenne = total / scores.size();
                        userRef.update("score", moyenne);
                        Toast.makeText(itemView.getContext(), "Merci d'avoir noté ce trajet !", Toast.LENGTH_LONG).show();
                        EvaluateAdapter adapter = new EvaluateAdapter(itemView.getContext(), list);
                        adapter.removeItem(getAdapterPosition());
                    }
                });
            }
        });
    }
}
