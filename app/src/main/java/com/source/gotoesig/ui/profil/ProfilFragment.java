package com.source.gotoesig.ui.profil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.source.gotoesig.R;
import com.source.model.User;
import com.squareup.picasso.Picasso;

public class ProfilFragment extends Fragment {

    private ProfilViewModel profilViewModel;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private static final String TAG = "ProfilFragment";

    private TextView dateDeDepart;
    private TextView prix;
    private TextView tempsTrajet;
    private TextView vehicule;
    private TextView adresseDepart;
    private TextView adresseArrivee;
    private TextView nbrAvis;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        profilViewModel =
                ViewModelProviders.of(this).get(ProfilViewModel.class);
        View root = inflater.inflate(R.layout.fragment_profil, container, false);
        final TextView textView = root.findViewById(R.id.text_profil);
        final TextView userName = root.findViewById(R.id.name);
        final TextView score = root.findViewById(R.id.score);
        final TextView mail = root.findViewById(R.id.mail);
        final ImageView picture = root.findViewById(R.id.picture);
        final TextView city = root.findViewById(R.id.ville);
        final TextView phone = root.findViewById(R.id.phone);
        final TextView nbrAvis= root.findViewById(R.id.nbrAvisProfil);

        profilViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
                mAuth = FirebaseAuth.getInstance();
                db = FirebaseFirestore.getInstance();
                DocumentReference docRef = db.collection("users").document(mAuth.getCurrentUser().getUid());
                docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        // Transforme data en objet User
                        User user = documentSnapshot.toObject(User.class);
                        int nbrAviss = user.getUserTrajetEvaluate().size() -1 ;
                        // On set dans la vue
                        userName.setText(user.getFirstName() + " " + user.getLastName()); //set text for text view
                        mail.setText(user.getEmail());
                        score.setText(String.valueOf(user.getScore()) + "/5" );
                        mail.setText(user.getEmail());
                        city.setText(user.getVille());
                        phone.setText(user.getPhone());
                        nbrAvis.setText(Integer.toString(nbrAviss) + " avis");
                        String imageUrl = user.getPictureUrl();
                        Picasso.get().load(imageUrl).into(picture);
                    }
                });
            }
        });
        return root;
    }
}