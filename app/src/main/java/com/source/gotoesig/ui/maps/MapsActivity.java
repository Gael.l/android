package com.source.gotoesig.ui.maps;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.source.gotoesig.Main2Activity;
import com.source.gotoesig.R;
import com.source.model.Trajet;
import com.source.model.User;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private FirebaseFirestore db;
    private FirebaseAuth mauth;
    private FragmentRefreshListener fragmentRefreshListener;
    private GoogleMap mMap;
    private Toolbar toolbar;
    private Polyline mPolyline;
    private List<String> particiapantsId = new ArrayList<String>();
    ArrayList<LatLng> mMarkerPoints;
    private static final String TAG = "MapsActivity";
    private Double latiA;
    private Double longiA;
    private Double latiD;
    private Double longiD;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        /** Declaration des éléments de la vue */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Attaching the layout to the toolbar object
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        // Setting toolbar as the ActionBar with setSupportActionBar() call
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        final TextView retardDetail = (TextView) findViewById(R.id.retardDetail);
        final TextView dateDeDepart = (TextView) findViewById(R.id.dateDeDepart);
        final TextView heureDeDepartDetail = (TextView) findViewById(R.id.heureDeDepartDetail);
        final TextView heureDarriveeDetail = (TextView) findViewById(R.id.heureDarriveeDetail);
        final TextView adresseDepartDetail = (TextView) findViewById(R.id.adresseDepartDetail);
        final TextView adresseArriveeDetail = (TextView) findViewById(R.id.adresseArriveeDetail);
        final TextView nbrPlace = (TextView) findViewById(R.id.nbrPlace);
        final TextView prixPlace = (TextView) findViewById(R.id.prixPlace);
        final TextView conducteur2 = (TextView) findViewById(R.id.conducteur2);
        final TextView scoreConducteur2 = (TextView) findViewById(R.id.scoreConducteur2);
        final TextView nbrAvisConducteur2 = (TextView) findViewById(R.id.nbrAvisConducteur2);
        final TextView numero = (TextView) findViewById(R.id.numero);
        final TextView tempsTrajet = (TextView) findViewById(R.id.tempsTrajet);
        final TextView distanceTrajet = (TextView) findViewById(R.id.distanceTrajet);
        final TextView autorouteDetails = (TextView) findViewById(R.id.autorouteDetails);
        final Button choisirTrajet = (Button) findViewById(R.id.ButChoisirTrajet);
        heureDeDepartDetail.setText("08h00");
        heureDarriveeDetail.setText("08h30");

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mMarkerPoints = new ArrayList<>();

        /** Récupérer les donnés relatives au clic sur un trajet specific depuis l'adapter */
        db = FirebaseFirestore.getInstance();
        Log.d(TAG, "getIncomingIntent: checking for incoming intents.");
        if(getIntent().hasExtra("trajet_id")){
            Log.d(TAG, "getIncomingIntent: found intent extras.");
            final String trajetId = getIntent().getStringExtra("trajet_id");
            Log.d(TAG, "getIncomingIntent: " + trajetId);

            /** Récuperer le bon docucment associé au trjaet */
            DocumentReference docRef = db.collection("trajets").document(trajetId);
            docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    Trajet trajet = documentSnapshot.toObject(Trajet.class);

                    /** Fromatage des messages pour autoroute et place disponible */
                    // Seleon la valeur de isAutouroute on set une vealeur différencte
                    String autouroute = "";
                    if(trajet.isAutoroute()) autouroute = "Présence d'autouroute";
                    else autouroute = "Pas d'autouroute";

                    // Si il reste plus d'une place on met au pluriel
                    String placeDispo = "";
                    if(trajet.getNbrPlaces() > 1) placeDispo = " places disponibles";
                    else placeDispo = " place disponible";

                    /** Info relatives au trajet, récupération depuis Firestore*/
                    dateDeDepart.setText(getDateDepart(trajet.getDepart()));
                    heureDeDepartDetail.setText(getHeureDepart(trajet.getDepart()));
                    heureDarriveeDetail.setText(getHeureArrivee(trajet.getDepart(), trajet.getSecondes()));
                    retardDetail.setText( "Retard toléré : " + Integer.toString(trajet.getRetard()) + " minutes"); //set text for text view
                    adresseDepartDetail.setText(trajet.getAdresseDepart());
                    adresseArriveeDetail.setText(trajet.getAdresseArivee());
                    nbrPlace.setText(trajet.getNbrPlaces() + placeDispo);
                    prixPlace.setText(trajet.getPrix() + " €");
                    tempsTrajet.setText(trajet.getDuree());
                    distanceTrajet.setText(" - " + trajet.getDistance());
                    autorouteDetails.setText(autouroute);

                    /** Permet de tracer le trajet sur la carte */
                    drawRoute(trajet.getAdresseDepart(), trajet.getAdresseArivee());

                    /** Info relatives au conducteur */
                    DocumentReference docRef = db.collection("users").document(trajet.getCreateurId());
                    docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            // Transforme data en objet Trajet
                            User user = documentSnapshot.toObject(User.class);
                            conducteur2.setText(user.getFirstName());
                            scoreConducteur2.setText(String.valueOf(user.getScore()) + " / 5" );
                            nbrAvisConducteur2.setText(" - 15 avis");
                            numero.setText(user.getPhone());
                        }
                    });
                }
            });

            /** Lors du clic sur choisir ce trajet */
            choisirTrajet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mauth = FirebaseAuth.getInstance();
                    DocumentReference trajetRef = db.collection("trajets").document(trajetId);
                    DocumentReference userRef = db.collection("users").document(mauth.getUid());
                    trajetRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            Trajet trajet = documentSnapshot.toObject(Trajet.class);
                            trajetRef.update("nbrPlaces",  trajet.getNbrPlaces() - 1);
                            trajetRef.update("participants", FieldValue.arrayUnion(mauth.getUid()));
                            startActivity( new Intent(MapsActivity.this, Main2Activity.class));                     }
                    });
                    /** Retour à la page des trajets */
                    if(getFragmentRefreshListener()!= null){
                        getFragmentRefreshListener().onRefresh();

                    }
                }
            });
        }
    }

    /**
     * Permet d'instancier la carte google Map
     *
     * */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        }
    }

    /**
     * Permet de récupérer les donées depuis Google
     *
     * */
    private void drawRoute(String origin, String destination){
        // Getting URL to the Google Directions API
        DownloadTask downloadTask = new DownloadTask();
        String url = "https://maps.googleapis.com/maps/api/directions/json?origin="+
                origin+"&destination="+
                destination+
                "&key=AIzaSyBERFhpnWbFCaQaWYR5Hvu1sVJBVZdNknc&fbclid=IwAR2cW8BcT61jaMnnVveTbIC5vSmHLu0NYGdp9AkMxXK0DEF39vOIeDFA-2k";
        // Start downloading json data from Google Directions API
        downloadTask.execute(url);
    }

    /** Méthode qui permet de télécharger des données en format JSON depuis une URL */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("Exception on download", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    /** A class to download data from Google Directions URL */
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("DownloadTask","DownloadTask : " + data);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /** A class to parse the Google Directions in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            LatLngBounds.Builder builder = new LatLngBounds.Builder();

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                HashMap<String,String> pointD = path.get(0);
                latiD = Double.parseDouble(pointD.get("lat"));
                longiD = Double.parseDouble(pointD.get("lng"));

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    latiA = lat;
                    longiA=lng;

                    LatLng position = new LatLng(lat, lng);
                    builder.include(position);
                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(6);
                lineOptions.color(Color.BLUE);
            }

            // Drawing polyline in the Google Map for the i-th route
            if(lineOptions != null) {
                if(mPolyline != null){
                    mPolyline.remove();
                }
                mPolyline = mMap.addPolyline(lineOptions);
                // Zoom sur le trajet
                /**initialize the padding for map boundary*/
                int padding = 50;
                /**create the bounds from latlngBuilder to set into map camera*/
                LatLngBounds bounds = builder.build();
                /**create the camera with bounds and padding to set into map*/
                final CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latiD,longiD))
                .title("Départ "));

                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latiA,longiA))
                        .title("Arrivée "));

                /**call the map call back to know map is loaded or not*/
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(10, 10))
                        .title("Hello world"));
                mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        /**set animated zoom camera into map*/
                        mMap.animateCamera(cu);
                    }
                });
            }else
                Toast.makeText(getApplicationContext(),"No route is found", Toast.LENGTH_LONG).show();
        }
    }

    public String getDateDepart(Date dateDepart) {
        String dateDepartLettre = "";
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateDepart);
        int day = calendar.get(Calendar.DATE);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);
        int dow = calendar.get(Calendar.DAY_OF_WEEK);
        int dom = calendar.get(Calendar.DAY_OF_MONTH);
        int doy = calendar.get(Calendar.DAY_OF_YEAR);
        String jour=null;
        String mois=null;
        if(dow==1){
            jour = "Dimanche";
        }
        if(dow==2){
            jour = "Lundi";
        }
        if(dow==3){
            jour = "Mardi";
        }
        if(dow==4){
            jour = "Mercredi";
        }
        if(dow==5){
            jour = "Jeudi";
        }
        if(dow==6){
            jour = "Vendredi";
        }
        if(dow==7){
            jour = "Samedi";
        }
        if(month==1){
            mois = "Janvier";
        }
        if(month==2){
            mois = "Février";
        }
        if(month==3){
            mois = "Mars";
        }
        if(month==4){
            mois = "Avril";
        }
        if(month==5){
            mois = "Mai";
        }
        if(month==6){
            mois = "Juin";
        }
        if(month==7){
            mois = "Juillet";
        }
        if(month==8){
            mois = "Aout";
        }
        if(month==9){
            mois = "Septembre";
        }
        if(month==10){
            mois = "Octobre";
        }
        if(month==11){
            mois = "Novembre";
        }
        if(month==12){
            mois = "Décembre";
        }

        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        String zero ="";
        String Arriveezero ="";
        if(minute < 10 ) {
            zero="0";
        } else {
            zero="";
        }
        return jour+" "+day+" "+mois;
    }

    public String getHeureDepart(Date dateDepart) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateDepart);
        int hour = calendar.get(Calendar.HOUR_OF_DAY)+1;
        int minute = calendar.get(Calendar.MINUTE);
        String zero ="";
        if(minute < 10 ) {
            zero="0";
        } else {
            zero="";
        }
        return hour+"h"+zero+minute;
    }

    public String getHeureArrivee( Date dateDepart, int dureeTrajet) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateDepart);
        calendar.add(Calendar.SECOND,dureeTrajet);
        int hour = calendar.get(Calendar.HOUR_OF_DAY)+1;
        int minute = calendar.get(Calendar.MINUTE);
        String zero ="";
        if(minute < 10 ) {
            zero="0";
        } else {
            zero="";
        }
        return hour+"h"+zero+minute;
    }

    public FragmentRefreshListener getFragmentRefreshListener() {
        return fragmentRefreshListener;
    }

    public void setFragmentRefreshListener(FragmentRefreshListener fragmentRefreshListener) {
        this.fragmentRefreshListener = fragmentRefreshListener;
    }

    public interface FragmentRefreshListener{
        void onRefresh();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.maps, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }
}
